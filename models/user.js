const mongoose = require('mongoose');
const usersSchema = new mongoose.Schema({
    profileName: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    age: {
        type: String,
        required: true
    },
}); 
module.exports = mongoose.model('User', usersSchema);