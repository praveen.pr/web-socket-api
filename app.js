
const express = require('express');
const http = require('http');
const path = require('path');
const socket= require('socket.io');

const index = require('./routes/index');
const dbConnection = require('./models');
const app = express();
const PORT = 3005;
app.set('port', PORT);

(async function(){
  await dbConnection();
})();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs')
//view engine setup
 app.use('/', function(req, res){
  res.render('index');
 });

const server = http.createServer(app);

const io = socket(3002);
module.exports = io;
app.io = io;
require('./socket-connection')(io);

server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

module.exports = app;