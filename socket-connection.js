const dboperations = require('./Services/user-crud');
module.exports = function (io) {
  io.on('connection', function (socket) {
    console.log('Socket connected', socket.id)

    io.to(socket.id).emit("requestUserId", socket.id);

    socket.on('listUsers',async function () {
      const users = await dboperations.get();
      socket.emit('userData', users)
      
  })

    socket.on('createUser',async function (data) {
        await dboperations.create(data);
        socket.emit('alert', {status : "create" , message: "success"})
    })

    socket.on('deleteUser',async function (id) {
      await dboperations.delete(id);
      socket.emit('alert', {status : "delete" , message: "success"})
   })

   
   socket.on('editUser',async function ({id , user}) {
    await dboperations.update(id , user);
    socket.emit('alert', {status : "edit" , message: "success"})
  })

  socket.on('getUserData',async function (id) {
    const data = await dboperations.getById(id);
    socket.emit('editUser', data)
  })


  socket.on('userUpdateData',function (userid , data) {
      const userDetails = {
            email: data.email,
             profName: data.profName,
            age: data.age
      }
   })

    socket.on('disconnect', function () {
      console.log('Socket disconnected');
    });
  })
}
