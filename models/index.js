const mongoose = require('mongoose');

const MongoConnect = async () =>
{
    try{
         mongoose.connect( `mongodb://localhost:27017/gen-user`, { useNewUrlParser: true,useUnifiedTopology:true});
        console.log("DB connected!")
    }catch (e) {
         console.error("DB connection failed!",e)
    }
}

module.exports = MongoConnect;
