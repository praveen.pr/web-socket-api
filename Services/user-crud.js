const User = require('../models/user');

const crudOperations = {
    create : function(userObj){
        const user = new User(userObj);
        return user.save();
    },
    update : function(id , userObj){
        return User.update({_id: id} , userObj);
    },
    get : function(){
        return User.find().exec();
    },
    getById: function(id){
        return User.findById(id).exec();
    },
    delete: function(id){
        return User.find({ _id: id}).remove().exec();
    },
}
module.exports = crudOperations;